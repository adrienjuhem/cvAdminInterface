# cvAdminInterface

## details

An admin interface for my CV (resume).

## Technology

Made in NodeJs with template in ejs.

## Instructions

Run `cp config/db.exemple.js config/db.js` and fill it with your mysql login and db.

Run `npm install` to download npm dependencies.

Run `npm start` to run the app.



