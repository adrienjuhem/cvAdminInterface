let express = require('express');
let ejs = require('ejs');
let app = express();
let bodyParser = require('body-parser');
let session = require('express-session');
let flash = require('./middlewares/flash');

// Template
app.set('view engine', 'ejs');

//Midlewares
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}))
app.use(flash);

//Routes

//Home
app.get('/', (request, response) => {
    let Competence = require('./models/competence');
    Competence.getAll(function (competences) {
        response.render('index', { competences: competences });
    });
});

//Competences
app.get('/competences', (request, response) => {
    let Competence = require('./models/competence');
    Competence.getAll(function (competences) {
        response.render('./competence/all', { competences: competences });
    });
});

app.get('/competences/add', (request, response) => {
    console.log(request.session)
    response.render('./competence/form');
});

app.post('/competences/add', (request, response) => {
    console.log(request.body.nom);
    if (request.body.nom === undefined || request.body.nom === "") {
        request.flash('error', 'le champs compétence ne doit pas être vide');
        response.redirect('/competences/add');

    } else {
        let Competence = require('./models/competence');
        Competence.create(request.body, function () {
            request.flash('success', 'la  compétence a été ajouté avec succès');
            response.redirect('/competences');
        });
    }
});

app.get('/competences/:id', (request, response) => {
    let Competence = require('./models/competence');
    
    Competence.getOne(request.params.id, function (competence) {
        response.render('competence/show', { competence: competence, visible: false });
    });
});

app.get('/competences/:id/delete', (request, response) => {
    let Competence = require('./models/competence');

    Competence.delete(request.params.id, function () {
        request.flash('success', 'la  compétence a été supprimé');
        response.redirect('/competences');
    });
});

app.post('/competences/:id/update', (request, response) => {
   
    if (request.body.nom === undefined || request.body.nom === "") {
        request.flash('error', 'le champs compétence ne doit pas être vide');
        response.redirect('/competences/add');

    } else {
        let Competence = require('./models/competence');
        Competence.update(request.params.id, request.body, function () {
            request.flash('success', 'la  compétence a été mise à jour avec succès');
            response.redirect('/competences/'+request.params.id);
        });
    }
});

//Experiences
app.get('/experiences', (request, response) => {
    let Experience = require('./models/experience');
    Experience.getAll(function (experiences) {
        response.render('./experience/all', { experiences: experiences });
    });
});

app.get('/experiences/add', (request, response) => {
    console.log(request.session)
    response.render('./experience/form');
});

app.post('/experiences/add', (request, response) => {
    console.log(request.body.nom);
    if (request.body.poste === "" || request.body.entreprise === "" || request.body.date === "") {
        request.flash('error', 'le champs compétence ne doit pas être vide');
        response.redirect('/experiences/add');

    } else {
        let Experience = require('./models/experience');
        Experience.create(request.body, function () {
            request.flash('success', 'la  compétence a été ajouté avec succès');
            response.redirect('/experiences');
        });
    }
});

app.get('/experiences/:id', (request, response) => {
    let Experience = require('./models/experience');
    
    Experience.getOne(request.params.id, function (experience) {
        response.render('experience/show', { experience: experience, visible: false });
    });
});

app.get('/experiences/:id/delete', (request, response) => {
    let Experience = require('./models/experience');

    Experience.delete(request.params.id, function () {
        request.flash('success', 'la  compétence a été supprimé');
        response.redirect('/experiences');
    });
});

app.post('/experiences/:id/update', (request, response) => {
   
    if (request.body.poste === "" || request.body.entreprise === "" || request.body.date === "") {
        request.flash('error', 'le champs compétence ne doit pas être vide');
        response.redirect('/experiences/add');

    } else {
        let Experience = require('./models/experience');
        Experience.update(request.params.id, request.body, function () {
            request.flash('success', 'la  compétence a été mise à jour avec succès');
            response.redirect('/experiences/'+request.params.id);
        });
    }
});

// Books
app.get('/books', (request, response) => {
    let Book = require('./models/book');
    Book.getAll(function ( books) {
        response.render('./book/all', { books: books });
    });
});

app.get('/books/add', (request, response) => {
    console.log(request.session)
    response.render('./book/form');
});

app.post('/books/add', (request, response) => {
    if (request.body.title === "" || request.body.link === "" || request.body.image === "" || request.body.content === "") {
        request.flash('error', 'le champs compétence ne doit pas être vide');
        response.redirect('/books/add');

    } else {
        let Book = require('./models/book');
        Book.create(request.body, function () {
            request.flash('success', 'la  compétence a été ajouté avec succès');
            response.redirect('/books');
        });
    }
});

app.get('/books/:id', (request, response) => {
    let Book = require('./models/book');
    
    Book.getOne(request.params.id, function (book) {
        response.render('book/show', { book: book, visible: false });
    });
});

app.get('/books/:id/delete', (request, response) => {
    let Book = require('./models/book');

    Book.delete(request.params.id, function () {
        request.flash('success', 'la  compétence a été supprimé');
        response.redirect('/books');
    });
});

app.post('/books/:id/update', (request, response) => {
   
    if (request.body.title === "" || request.body.link === "" || request.body.image === "" || request.body.content === "") {
        request.flash('error', 'le champs compétence ne doit pas être vide');
        response.redirect('/books/add');

    } else {
        let book = require('./models/book');
        book.update(request.params.id, request.body, function () {
            request.flash('success', 'la  compétence a été mise à jour avec succès');
            response.redirect('/books/'+request.params.id);
        });
    }
});

app.listen(3000);