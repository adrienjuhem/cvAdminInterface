let connection = require('../config/db');

class Book {
    
    constructor (row) {
        this.row = row;
    }

    get id () {
        return this.row.id
    }

    get title () {
        return this.row.title
    }

    get content () {
        return this.row.content
    }

    get link () {
        return this.row.link
    }

    get image () {
        return this.row.image
    }

    static create (content, cb) {
        connection.query('INSERT INTO book SET title = ?, content = ?, link = ?, image = ?' , [content.title, content.content, content.link, content.image], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }
    static getAll (cb) {
        connection.query('SELECT * FROM book ' , function (err, rows) {
            if (err) throw err
            cb(rows.map((row) => new Book(row)));

          });
    }
    static getOne (id, cb) {
        connection.query('SELECT * FROM book WHERE id = ? LIMIT 1' , [id], function (err, rows) {
            if (err) throw err
            cb(new Book(rows[0]));

          });
    }

    static delete (id, cb) {
        connection.query('DELETE FROM book WHERE id = ?' , [id], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }

    static update (id, content, cb) {
        connection.query('UPDATE book SET title = ?, content = ?, link = ?, image = ? WHERE id = ?' , [content.title, content.content, content.link, content.image, id], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }
}

module.exports = Book;