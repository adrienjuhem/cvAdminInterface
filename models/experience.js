let connection = require('../config/db');

class Experience {
    
    constructor (row) {
        this.row = row;
    }

    get id () {
        return this.row.id
    }

    get poste () {
        return this.row.poste
    }

    get titleShort () {
        return this.row.titleShort
    }

    get date () {
        return this.row.date
    }

    get lieu () {
        return this.row.lieu
    }

    get url () {
        return this.row.url
    }

    get photo () {
        return this.row.photo
    }

    get resume () {
        return this.row.resume
    }

    get text () {
        return this.row.text
    }

    get type () {
        return this.row.type
    }

    get entreprise () {
        return this.row.entreprise
    }

    static create (content, cb) {
        connection.query('INSERT INTO experience SET poste = ?, titleShort = ?, date = ?, entreprise = ?, lieu = ?, url = ?, photo = ?, resume = ?, text = ?, type = ?' , [content.poste, content.titleShort, content.date, content.entreprise, content.lieu, content.url, content.photo, content.resume, content.text, content.type], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }
    static getAll (cb) {
        connection.query('SELECT * FROM experience ' , function (err, rows) {
            if (err) throw err
            cb(rows.map((row) => new Experience(row)));

          });
    }
    static getOne (id, cb) {
        connection.query('SELECT * FROM experience WHERE id = ? LIMIT 1' , [id], function (err, rows) {
            if (err) throw err
            cb(new Experience(rows[0]));

          });
    }

    static delete (id, cb) {
        connection.query('DELETE FROM experience WHERE id = ?' , [id], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }

    static update (id, content, cb) {
        connection.query('UPDATE experience SET poste = ?, titleShort = ?, date = ?, entreprise = ?, lieu = ?, url = ?, photo = ?, resume = ?, text = ?, type = ? WHERE id = ?' , [content.poste, content.titleShort, content.date, content.entreprise, content.lieu, content.url, content.photo, content.resume, content.text, content.type, id], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }
}

module.exports = Experience;