let connection = require('../config/db');

class Competence {
    
    constructor (row) {
        this.row = row;
    }

    get id () {
        return this.row.id
    }

    get nom () {
        return this.row.nom
    }

    get type () {
        return this.row.type
    }

    get niveau () {
        return this.row.niveau
    }

    static create (content, cb) {
        connection.query('INSERT INTO competence SET nom = ?, niveau = ?, type = ?' , [content.nom, content.niveau, content.type], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }
    static getAll (cb) {
        connection.query('SELECT * FROM competence ' , function (err, rows) {
            if (err) throw err
            cb(rows.map((row) => new Competence(row)));

          });
    }
    static getOne (id, cb) {
        connection.query('SELECT * FROM competence WHERE id = ? LIMIT 1' , [id], function (err, rows) {
            if (err) throw err
            cb(new Competence(rows[0]));

          });
    }

    static delete (id, cb) {
        connection.query('DELETE FROM competence WHERE id = ?' , [id], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }

    static update (id, content, cb) {
        connection.query('UPDATE competence SET nom = ?, niveau = ?, type = ? WHERE id = ?' , [content.nom, content.niveau, content.type, id], function (err, rows) {
            if (err) throw err
            cb(rows);

          });
    }
}

module.exports = Competence;